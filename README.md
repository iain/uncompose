`uncompose` is a mini compose wrapper for `nerdctl`. Note that
`nerdctl` is like `docker`.

This was required because nerdctl’s own compose implementation did not
originally support volume bind mounts. Now that it does, uncompose is
no longer required.
